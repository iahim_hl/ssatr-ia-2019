
package lab1.g2;


public class PanouComandaExterior {
    
    private Controler c;
    private int etajCurent;

    public PanouComandaExterior(Controler c, int etajCurent) {
        this.etajCurent = etajCurent;
        this.c = c;
    }
    
    
    void cheamaLift(){
        c.cheamaLift(etajCurent);
    }
    
}
