package lab1.g2;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Lift {
   private int etaj;
   
   Lift(int etaj){
       this.etaj = etaj;
   }
   
   void urca(){
       etaj++;
       try {
           Thread.sleep(1000);
        } catch (InterruptedException ex) {}
       System.out.println("Liftul la etajul "+etaj);
   }
   
   void coboara(){
       etaj--;
       try {
           Thread.sleep(1000);
        } catch (InterruptedException ex) {}
      
       System.out.println("Liftul la etajul "+etaj);
   }
   
   int getEtaj(){
       return etaj;
   }
}
