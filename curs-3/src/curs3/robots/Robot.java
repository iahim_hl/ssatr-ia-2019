
package curs3.robots;

public class Robot extends Thread {
    private String name;

    public Robot(String name) {
        this.name = name;
        setName(name);
        setDaemon(true);
    }
   
    public void run(){
        while(true){
            System.out.println("Robot "+name+" is moving...");
            try{Thread.sleep(1000);}catch(Exception e){}
        }
    }
    
    
}
