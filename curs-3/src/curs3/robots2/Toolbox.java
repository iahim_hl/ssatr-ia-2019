
package curs3.robots2;

public class Toolbox {
    
    int k = 1;
    
    public void useTool(){
        k--;
        System.out.println("Tool is in use!");
        displayStatus();
    }
    
    public void releaseTool(){
        System.out.println("Tool has been released!");
        k++;
        displayStatus();
    }
    
    public void displayStatus(){
        System.out.println("Usage status k="+k);
    }
    
    
}
