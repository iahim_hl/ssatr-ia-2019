
package curs3.robots2;

import curs3.robots.*;

public class RobotV2 extends Thread {
    private String name;
    private Toolbox tool;

    public RobotV2(String name, Toolbox tool) {
        this.name = name;
        setName(name);
        this.tool = tool;
    }
   
    public void run(){
        while(true){
            System.out.println("Robot "+name+" is moving...");
            synchronized(tool){
                tool.useTool();
                try{Thread.sleep(1000);}catch(Exception e){}
                tool.releaseTool();
            }
        }
    }
    
    
}
