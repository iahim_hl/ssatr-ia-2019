package lab2.g1;

public class Robot {
    private int x;
    private int y;
    private String id;

    public Robot(String id, int x, int y) {
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    /**
     * Sa se implementeze metoda move pentru a deplasa robotul pe una 
     * din directiile N S E V cu o unitate.
     * @param dir 
     */
    public void move(String dir){
        
    }

    @Override
    public String toString() {
        return "Robot{" + "x=" + x + ", y=" + y + '}';
    }
    
    
}
