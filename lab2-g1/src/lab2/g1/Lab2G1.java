/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2.g1;

/**
 *
 * @author mihai.hulea
 */
public class Lab2G1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Robot r1 = new Robot("A", 1, 1);
        r1.move("e");
        System.out.println(r1);
        r1.move("s");
        System.out.println(r1);
        
        RobotBoard b = new RobotBoard();
        b.addRobot(r1);
        b.display();
        Robot r2 = new Robot("B",3,2);
        b.addRobot(r2);
        b.display();
        
    }
    
}
