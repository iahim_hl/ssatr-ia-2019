
package curs.ssatr.database;

import java.sql.*;

public class DatabaseExample {
    public static void main(String[] args) throws Exception {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        System.out.println("JDBC driver loaded.");
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school","app","app");
        System.out.println("Connected to database!");
        Statement st = con.createStatement();
        System.out.println("Execute query");

        ResultSet rs = st.executeQuery("SELECT * FROM students");
        while(rs.next()){
            String name = rs.getString(2);
            String id = rs.getString(1);
            System.out.println("ID = "+id+" Name = "+name);
        }

        st.executeUpdate("INSERT INTO STUDENTS (ID, \"NAME\") \n" +
"	VALUES (3, 'Dan')\n" +
"");
        
        con.close();
        System.out.println("Database connection closed.");
    }
   
}
