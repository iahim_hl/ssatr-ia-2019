package ssatr.curs1.ex2;

public class Senzor {
    private static int v;
    private String l;

    public Senzor(int v, String l) {
        this.v = v;
        this.l = l;
    }

    public Senzor(int v) {
        this.v = v;
        this.l = "NONE";
    }

    void adaugaSiAfiseaza(){
        v = v + 1;
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Senzor{" +
                "v=" + v +
                ", l='" + l + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Senzor s1 = new Senzor(78, "AAA");
        Senzor s2 = new Senzor(56);
        Senzor s3;

        System.out.println(s1);
        System.out.println(s2);


        s1.adaugaSiAfiseaza();
        s1.adaugaSiAfiseaza();

        s1 = s2;
        //..................................
        System.out.println(s1);

        s1.adaugaSiAfiseaza();
        System.out.println(s2);


    }
}
