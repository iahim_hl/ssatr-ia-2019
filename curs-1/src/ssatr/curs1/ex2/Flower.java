package ssatr.curs1.ex2;

public class Flower {
    static int count;
    private int petals;

    public Flower(int petals) {
        this.petals = petals;
        count++;
    }

    static void displayNrofFlowers(){
        System.out.println("NRF = "+count);
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(18);
        Flower f2 = new Flower(18);
        Flower f3 = new Flower(18);

        Flower.displayNrofFlowers();

        f1.displayNrofFlowers();

    }
}
