
package lab1.g1.ex1;


public class Lab1G1Ex1 {


    public static void metoda1(int x){
        System.out.println("Rezolvare prin metoda 1.");
        if(x==0)
            System.out.println("ZERO");
        else if(x==1)
            System.out.println("UNU");
        else if(x==2)
            System.out.println("DOI");
        else
            System.out.println("Valoare necunoscuta.");
    }
    
    public static void metoda2(int x){
        System.out.println("Rezolvare prin metoda 2.");
        switch(x){
            case 0: System.out.println("ZERO");break;
            case 1: System.out.println("UNU");break;
            case 2: System.out.println("DOI");break;
            default: System.out.println("Valoare necunoscuta.");
        }
    }
    
    
    public static void main(String[] args) {
        metoda1(1);
        metoda2(3);
    }
    
}
