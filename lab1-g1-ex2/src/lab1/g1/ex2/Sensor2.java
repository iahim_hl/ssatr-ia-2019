
package lab1.g1.ex2;

public class Sensor2 {
    private int value; 
    private String location; 
    
    Sensor2(int value, String location){
        this.value = value;
        this.location = location;
    }
    
    Sensor2(String location){
        this.location = location;
        this.value = -1;
    }
    
    void display(){
        System.out.println("Sensor "+this.value+" located "+this.location);
    }
    
    void increment(){
        value++;
    }
    
    public static void main(String[] args) {
        Sensor2 s1 = new Sensor2("A");
        Sensor2 s2 = new Sensor2(10, "B");
        Sensor2 s3;
        s3 = s2;
      //  s1 = s2;
        System.out.println(s1);
        System.out.println(s2);
        s1.increment();
        s1.display();
        s2.increment();
        s2.display();
    
    }
}
