
package lab1.g1.ex2;


public class Controller {
    private Sensor senzorTemp;

    public Controller(Sensor senzorTemp) {
        this.senzorTemp = senzorTemp;
    }
    
    void controlStep(){
        if(senzorTemp.readValue()>28)
            System.out.println("Start cooling system. Reason: "+senzorTemp.readValue());
        else if(senzorTemp.readValue()<15)
            System.out.println("Start heating system.Reason: "+senzorTemp.readValue());
        else 
            System.out.println("DO nothing! Reason: "+senzorTemp.readValue());
    }
}
