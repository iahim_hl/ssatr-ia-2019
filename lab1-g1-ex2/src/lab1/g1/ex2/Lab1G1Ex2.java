/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1.g1.ex2;

import java.util.Random;

/**
 *
 * @author mihai.hulea
 */
public class Lab1G1Ex2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Sensor s1 = new Sensor(10, "Garden");
        Sensor s2 = new Sensor(10, "Garden");
        
        System.out.println(s1);
        System.out.println(s2);
        
        if(s1 == s2)  // nu se compara obiectele folosin equals!!!
            System.out.println("Sensors equals!");
        else
            System.out.println("Sensors not equals!"); 
       
        if(s1.equals(s2))  // nu se compara obiectele folosin equals!!!
            System.out.println("Sensors equals!");
        else
            System.out.println("Sensors not equals!"); 
       
        
        Random r = new Random();
        
        Controller c1 = new Controller(s1);
        
        s1.setValue(r.nextInt(50));
        c1.controlStep();
        s1.setValue(r.nextInt(50));
        c1.controlStep();
        
        Controller c2 = new Controller(s2);
        s2.setValue(r.nextInt(50));
        c2.controlStep();
        s2.setValue(r.nextInt(50));
        c2.controlStep();
    }
    
}
