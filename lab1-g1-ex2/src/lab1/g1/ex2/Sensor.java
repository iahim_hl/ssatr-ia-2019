
package lab1.g1.ex2;

import java.util.Objects;

public class Sensor {
    private int value;
    private String location;

    public Sensor(int value, String location) {
        this.value = value;
        this.location = location;
    }
    
    int readValue(){
        return value;
    }
    
    void setValue(int x){
        this.value = x;
    }

    public String toString() {
         return "Sensor value is "+value+" and location is "+location;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
   
        final Sensor other = (Sensor) obj;
        return this.value == other.value && this.location.equals(other.location);

    }
    
    
    
    
}
