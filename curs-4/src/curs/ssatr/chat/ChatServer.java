/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package curs.ssatr.chat;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatServer extends Thread{
    ArrayList<ClientHandler> clients = 
            new ArrayList<>();
    
    public void run(){
        try {
            ServerSocket ss = new ServerSocket(7878);
            while(true){               
                System.out.println("Waiting for clients...");
                Socket socket = ss.accept();
                System.out.println("Client connected!");
                ClientHandler h = new ClientHandler(socket, this);
                h.start();
                clients.add(h);
            }
        } catch (Exception ex) {
            Logger.getLogger(ChatServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void sendToAll(String senderName,  String msg){
//        for(ClientHandler c: clients){
//            if(c.isActive())
//                c.sendMessage(msg);
//        
//        }
        
        //Mihai:#Alin Ce mai faci ? 
        //Mihai: Ce mai faci?
        String to = null;
        if(msg.contains("#")){
            to = msg.substring(msg.indexOf("#")+1);
            //Alin Ce mai faci ?
            to = to.substring(0, to.indexOf(" "));
            System.out.println("SEND TO:"+to+".");
        }
        
        Iterator<ClientHandler> iter = clients.iterator();
        while(iter.hasNext()){
            ClientHandler c = iter.next();
            if(!c.isActive())
                iter.remove();
            else 
                if(!senderName.equals(c.getMyName()))
                    if(to==null)
                        c.sendMessage(msg);
                    else if(c.getMyName().equals(to))
                        c.sendMessage(msg);
                        
        }
    }
    
     public static void main(String[] args) {
        ChatServer srv = new ChatServer();
        srv.start();
    }
    
}


