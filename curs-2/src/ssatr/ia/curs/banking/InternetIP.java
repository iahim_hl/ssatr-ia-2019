/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssatr.ia.curs.banking;


public class InternetIP {
    private static InternetIP ip;
    private String value;

    private InternetIP(String value) {
        this.value = value;
    }
    
    public static InternetIP getInstance(String value){
       if(ip==null)
           ip = new InternetIP(value);
       return ip;
    }

    @Override
    public String toString() {
        return "InternetIP{" + "value=" + value + '}';
    }
    
 
}
