
package ssatr.ia.curs.banking;

import java.util.ArrayList;
import java.util.Random;

public class AccountsManager {
    private ArrayList<BankAccount> accounts 
            = new ArrayList<>();
    
    public void transfer(int amount, int idS, int idR){
        BankAccount s = findById(idS);
        BankAccount r = findById(idR);
        if(s==null||r==null){
            System.out.println("Account not found for sender or receiver.");
            return;
        }
        
        if(s.withdraw(amount))
            r.deposit(amount);
            
    }
    
    public BankAccount findById(int id){
        for(int i=0;i<accounts.size();i++)
            if(accounts.get(i).getId() == id)
                return accounts.get(i);
        return null;
    }
    
    Card associateCard(int ownerId){
        Random r = new Random();
        BankAccount a = findById(ownerId);
        if(a!=null){         
            Card c = new Card(r.nextInt(1000), ownerId, 0);
            a.setCard(c);
            return c;
        }
        return null;
    }

    void addAccount(BankAccount b) {
        accounts.add(b);
    }
    
     public void displayCardOwner(int cid, int pin){
       // implementare functie pentru gasire si afisare proprietar card
    }
    
}
