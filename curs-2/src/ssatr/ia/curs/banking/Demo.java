/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssatr.ia.curs.banking;

/**
 *
 * @author mihai.hulea
 */
public class Demo {
    public static void main(String[] args) {
        Address a = new Address("Baritiu", 16);
        BankAccount b1 = new BankAccount("Alin", 5242, a);
//        b1.deposit(100);
//        b1.withdraw(20);
//        b1.deposit(-90);
        
        BankAccount b2 = new BankAccount("Dan", 123, a);
        BankAccount b3 = new BankAccount("Vali", 234, a);
        BankAccount b4 = new BankAccount("Alina", 567, a);
        
        AccountsManager am = new AccountsManager();
        
        am.addAccount(b1);
        am.addAccount(b2);
        am.addAccount(b3);
        am.addAccount(b4);
        
        am.transfer(10, 123, 567);
        
        Card x = am.associateCard(567);
        am.displayCardOwner(x.getId(), 0);
        
        ///////singleton demo
        
  
//        InternetIP ip1 = new InternetIP("127.0.0.1");
//        InternetIP ip2 = new InternetIP("127.0.0.1");
//        InternetIP ip3 = new InternetIP("127.0.0.1");
//          InternetIP ip1 = InternetIP.getInstance("127.0.0.1");
//          InternetIP ip2 = InternetIP.getInstance("127.0.0.1");
//          InternetIP ip3 = InternetIP.getInstance("127.0.0.1");
          
    }
}
