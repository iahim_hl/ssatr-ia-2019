
package ssatr.ia.curs.banking;

public class Person {
    protected String name;
    private int id;
    private Address adr;

    public Person(String name, int id, Address adr) {
        this.name = name;
        this.id = id;
        this.adr = adr;
    }
    
    public int getId(){
        return id;
    }
    
    public String getName(){
        return name;
    }
}
