
package ssatr.ia.curs.banking;

public class Address {
    private String name;
    private int number;

    public Address(String name, int number) {
        this.name = name;
        this.number = number;
    }
    
    public void display(){
        System.out.println("Address is "+name+" "+number);
    }
    
}
