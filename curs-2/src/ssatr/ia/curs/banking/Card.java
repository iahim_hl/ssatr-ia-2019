package ssatr.ia.curs.banking;

public class Card {
    private int id;
    private int ownerId;
    private int pin;

    public Card(int id, int ownerId, int pin) {
        this.id = id;
        this.ownerId = ownerId;
        this.pin = pin;
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public int getPin() {
        return pin;
    }
    
    
}
