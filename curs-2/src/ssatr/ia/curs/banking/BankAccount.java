
package ssatr.ia.curs.banking;

public class BankAccount extends Person {
    private int amount;
    private Card card;
    
    public BankAccount(String name, int id, Address adr) {
       super(name, id, adr);
       deposit(100);
    }
    
    public void setCard(Card c){
        this.card = c;
    }
    
    public Card getCard(){
        return card;
    }
    
    boolean deposit(int v){
        if(v>0){
            amount+=v;
            System.out.println("New deposit in "+this); 
            return true;
        }
        System.out.println("New deposit in "+this);            
        return false;
    }
    
    boolean withdraw(int v){
        if(amount-v>=0){
            amount-=v;
            System.out.println("Withdraw from deposit "+this);  
            return true;
        }
        System.out.println("Withdraw from deposit "+this);            
        return false;
    }
    

    @Override
    public String toString() {
        return "BankAccount{" + "amount=" + amount + " owner="+name+ '}';
    }
    
    
}
